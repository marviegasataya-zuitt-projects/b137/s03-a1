package b137.gasataya.s03a1;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        System.out.println("Factorial\n");

        Scanner appScanner = new Scanner(System.in);

        // Activity:
        // Create a Java program that accepts an integer and computes for
        // the factorial value and displays it to the console.
        System.out.println("Enter a Number:\n");
        int number = appScanner.nextInt();

        long result = 1;
        
        for (int i = 1; i <= number; i++) {
            result = result * i;
        }

        System.out.println("Factorial of " + number + " is: " + result);

    }
}
